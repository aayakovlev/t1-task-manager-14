package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Command;

public interface CommandService {

    Command[] getCommands();

}
