package ru.t1.aayakovlev.tm.service;

import ru.t1.aayakovlev.tm.model.Task;

import java.util.List;

public interface TaskService extends BaseService<Task> {

    Task create(final String name);

    Task create(final String name, final String description);

    List<Task> findByProjectId(final String projectId);

    Task updateById(final String id, final String name, final String description);

    Task updateByIndex(final Integer index, final String name, final String description);

}
