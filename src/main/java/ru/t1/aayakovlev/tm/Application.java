package ru.t1.aayakovlev.tm;

import ru.t1.aayakovlev.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
