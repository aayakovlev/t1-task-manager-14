package ru.t1.aayakovlev.tm.controller;

public interface ProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}
