package ru.t1.aayakovlev.tm.controller;

public interface BaseController {

    void changeStatusById();

    void changeStatusByIndex();

    void clear();

    void completeById();

    void completeByIndex();

    void create();

    void removeById();

    void removeByIndex();

    void showAll();

    void showById();

    void showByIndex();

    void startById();

    void startByIndex();

    void updateById();

    void updateByIndex();

}
