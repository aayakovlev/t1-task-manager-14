package ru.t1.aayakovlev.tm.comparator;

import ru.t1.aayakovlev.tm.model.HasStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<HasStatus> {

    INSTANCE;

    @Override
    public int compare(final HasStatus o1, final HasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStatus() == null || o2.getStatus() == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
